const defaultState = {
    q: 'redux',
    books: {},
}

const mainReducer = (state = defaultState, action) => {
    switch(action.type) {
        case 'GET_BOOKS':
            return action;
        case 'GET_BOOK':
            return {...state, book: action.book};
        case 'RESET_BOOK_SELECTION':
            return {...state, book: null};
        default:
            return state;
    }
}

export default mainReducer;