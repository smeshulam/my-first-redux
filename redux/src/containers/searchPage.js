import React, { Component } from 'react';
import { bindActionCreators, Dispatch } from "redux";
import Search from '../components/search';
import { connect } from 'react-redux';
import * as actions from '../actions';

class SearchPage extends Component{

    handleInput(e) {
        const q = e.target.value;
        this.props.dispatch(actions.searchBooks(q));
    }

    render() {
        let items = {};
        return (
            <Search handleInput={this.handleInput.bind(this)} books={this.items}/>
        );
    }
}

const mapStateToProps = state => {
    return state;
}

export default connect(mapStateToProps)(SearchPage);