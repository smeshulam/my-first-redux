import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import App from './App';
import mainReducer from '../src/reducers/index';
import promiseMiddleware from 'redux-promise';
import * as serviceWorker from './serviceWorker';
import { composeWithDevTools } from 'redux-devtools-extension';
import './index.css';

const store = createStore(mainReducer, composeWithDevTools(applyMiddleware(promiseMiddleware, thunk)));

ReactDOM.render (
    <Provider store = { store }>
        <App />
    </Provider>
    , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
