
import React, { Component } from 'react';

const Search = (props) => {
    return (
            <div className="search_wrapper">
                <input
                    name="search"
                    className="search__input"
                    placeholder="Search for a book"
                    onInput = {props.handleInput}
                />
            </div>
        )
};

export default Search;