import React, { Component } from 'react';
import {connect} from "react-redux";
import * as actions from '../actions';

class Books extends Component {
    componentDidMount() {
        this.props.dispatch(actions.searchBooks(this.props.q));
    }

    // Returns list of books HTMLelements
    createSearchList() {
        let list =[];
        for(let i = 0; i < this.props.books.length; i++) {
           const  html = <li key={i} onClick={() => this.chooseBook(i)}>{this.props.books[i].volumeInfo.title}</li>; 
           list.push(html);
        }
        return list;
    }

    // Dispatches when book is clicked
    chooseBook(index) {
      this.props.dispatch(actions.getBook(this.props.books[index]));
    }

    // Reset selected book - Shows the list
    resetSelected() {
      this.props.dispatch(actions.resetSelection());
    }

    // Retruns book HTMLelements
    showBook(index) {
      const html = [];
      html.push(<h1>{this.props.book.volumeInfo.title}</h1>);
      html.push(<h2>{this.props.book.volumeInfo.subTitle}</h2>);
      html.push(<p>{this.props.book.volumeInfo.description}</p>);
      html.push(<a href={this.props.book.volumeInfo.previewLink} target="_blank">Preview Link</a>);
      html.push(<button onClick={() => this.resetSelected()}>Back</button>);
      return html;
    }

    render() {
       if(!this.props.book) {
        return (
        // if books found for the query
         <div>
             <ul>
                {this.createSearchList()}
             </ul>
         </div>
        )
       }else if(this.props.book) {
         // book is selected - show the book
        return (
          <div>
            {this.showBook()}
          </div>
        )
       }else {
        return (
            // if no books found for the query
            <div>
                no books found
            </div>
        )
       }
    }
}

const mapStateToProps = (state) => {
    return state;
};

export default connect(mapStateToProps)(Books);