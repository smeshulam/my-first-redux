import React, { Component } from 'react';
import SearchPage from './containers/searchPage';
import Books from './components/books';
import './App.css';

class App extends Component {
  handleInput(e) {
  }
  
  render() {
    return (
      <div className="App">
        <SearchPage/>
        <Books/>
      </div>
    );
  }
}

export default App;
