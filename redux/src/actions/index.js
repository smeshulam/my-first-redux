import axios from 'axios';

const GOOGLE_BOOKS_API_URL = 'https://www.googleapis.com/books/v1/volumes';

export function resetSelection() {
    return {
        type: 'RESET_BOOK_SELECTION'
    }
}

export function getBook(book) {
    return {
        type: 'GET_BOOK',
        book: book,
    }
}

export function getBooks(q, data) {
    return {
        type: 'GET_BOOKS',
        q: q,
        totalItems: data.totalItems,
        books: data.items
    }
}

export function searchBooks(q) {
    return(dispatch) => { 
        return  axios.get(`${GOOGLE_BOOKS_API_URL}?q=${q}`)
            .then(response => response.data)
            .then(data => dispatch(getBooks(q, data)))
            .catch(e => console.log(e))
    }
}
    

export const actions = {
    searchBooks: searchBooks,
    getBook: getBook,
    resetSelection: resetSelection
}